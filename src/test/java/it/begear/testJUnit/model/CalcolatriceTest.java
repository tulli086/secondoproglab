package it.begear.testJUnit.model;

import static org.junit.Assert.*;

import org.junit.Assert;
import org.junit.Test;

public class CalcolatriceTest {

	@Test
	public void testSomma() {
		Calcolatrice calcolatrice = new Calcolatrice();
		int a= calcolatrice.somma(10,10);
		Assert.assertEquals(20, a);
		
	}

	@Test
	public void testDifferenza() {
		Calcolatrice calcolatrice= new Calcolatrice();
		int b=  calcolatrice.differenza(20, 10);
		Assert.assertEquals(10,b);
	
	}

	@Test
	public void testProdotto() {
	Calcolatrice calcolatrice= new Calcolatrice();
	double b= calcolatrice.prodotto(10, 2);
	Assert.assertEquals(20, b, 0.1);
	
	
	}

	@Test
	public void testDivisione() {
		Calcolatrice calcolatrice = new Calcolatrice();
		double c= calcolatrice.divisione(10, 2);
		Assert.assertEquals(5, c, 0.1);
	
	}

}
