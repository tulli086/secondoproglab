package it.begear.testJUnit.controller;

import it.begear.testJUnit.model.Calcolatrice;

public class Controller {

	public static void main(String[] args) {

	Calcolatrice calcolatrice= new  Calcolatrice();
		
	int somma =	calcolatrice.somma(10, 10);
	
	int differenza = calcolatrice.differenza(20, 10);
	
	double prodotto = calcolatrice.prodotto(4, 2);
	
	double divisioni = calcolatrice.divisione(8, 2);
	
	System.out.println("somma: " + somma + " "+ "differenza: " + differenza + " " + " prodotto: " + prodotto +  " divisioni: " + divisioni);
		
	
	}

}
