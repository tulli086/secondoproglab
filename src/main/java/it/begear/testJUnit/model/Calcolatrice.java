package it.begear.testJUnit.model;

public class Calcolatrice {

	public int somma(int a,int b) {
		
		return a+b;
	}
	
	public int differenza(int a,int b) {
		
		return a-b;
	}
	
	public double prodotto(double a,double b) {
		
		return a*b;
	}
	
	public double divisione(double a, double b) {
		
		return a/b;
	}
	
}
